package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        //переберем все элементы коллекции Y - (elem), будем сравнивать их с элементами коллекции X - (x.get(i)),
        //индекс i изначально будет равен нулю, далее в случае равенства elem и x.get(i), i - будет увеличиваться на 1,
        //так же будет увеличиваться на один переменная size2, которая
        //показывает сколько элементов коллекции Y совпадают с элементами X
        //если по окончании цикла выражение x.size() == size2 будет давать истину, значит можно получить последовательность X
        //путем вычеркивания некоторых элементов из Y
        //цикл может прерватся раньше, когда итератор i достигнет размера коллекции X
        //это будет обозначать, что в проверенной части коллекции Y уже есть все необходимые элементы, следовательно дальнейшая проверка
        //не имеет смысла и метод может вернуть значение true
        if (x==null||y==null)throw new IllegalArgumentException();


        if (x.isEmpty()) {
            return true;
        }

        int size2 = 0;
        int i = 0;

        for (Object elem : y) {
            if (i >= x.size()) {
                return true;
            }

            if (elem.equals(x.get(i))) {

                size2++;
                i++;
            }
        }
        return x.size() == size2;
    }

}
