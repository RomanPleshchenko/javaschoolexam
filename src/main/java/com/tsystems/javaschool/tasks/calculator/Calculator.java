package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
     //21.02.2017

    public ArrayList<String> rpnArray;// массив для хранения обратной польской строки
    public ArrayList<String> tempArray;// вспомогательный массив

    int NumOperation;
    String[] myOperators = {"+", "-", "*", "/", "(", ")","|"};// арифметические операции согласно заданию плюс служебная строка "|", символизирующая о начале и кончании получаемых данных

    public String evaluate(String statement) {

        if(statement==null)return null;

        //Алгоритм расчета построен на использованием обратной польской строки,(далее - ОПС),теория её построения взята с
        //ресурса http://master.virmandy.net/perevod-iz-infiksnoy-notatsii-v-postfiksnuyu-obratnaya-polskaya-zapis/

        rpnArray = new ArrayList();//массив для хранения самой польской записи в виде коллекции
        tempArray = new ArrayList<String>();// вспомогательная коллекция

        //проверимм одинаковое ли количество закрывающих и открывающих скобок, если нет возвращает null
        char[] chars = statement.toCharArray();
        int balance = 0;
        for (char c:chars) {
            if(c=='(')balance++;
            if(c==')')balance--;
        }
        if (balance!=0)return null;



        //разберем подаваемую строку в массив,"|"- добавлен чтобы по нему понять об окончании строки
        statement = (statement + "|").replace(" ", "");
        for (String myOperator : myOperators) {
            statement = statement.replace(myOperator, " " + myOperator + " ");
        }
        String units[] = statement.split(" ");

        // переберу все операторы и операнды так чтобы они согласно правилам сформировали польскую строку

        for (String unit : units) {

            //пробелы просто пропускаем
            if (unit.equals("")) {
                continue;
            }

            if (fillArrays(unit) == 0) {
                return null;
            }

        }

        //теперь когда ОПС собрано вычислим результат выражения

        Stack<Double> stackDouble = new Stack<Double>();

        for (String elem : rpnArray) {

            if (isOperand(elem)) {

                stackDouble.add(Double.parseDouble(elem));

            }else{

                double y = stackDouble.pop();
                double x = stackDouble.pop();

                if (elem.equals("+")) {
                    stackDouble.add(x + y);
                } else if (elem.equals("-")) {
                    stackDouble.add(x - y);
                } else if (elem.equals("*")) {
                    stackDouble.add(x * y);
                } else if (elem.equals("/")) {
                    stackDouble.add(x / y);
                }

            }

        }


        Double resault = 0.0;
        try {
            resault = stackDouble.pop();
        }catch (EmptyStackException e){
            return null;
        }

        //теперь округлим
        try {
            resault = BigDecimal.valueOf(resault).setScale(4,BigDecimal.ROUND_HALF_DOWN).doubleValue();
        }catch (Exception e){
            return null;
        };


        String res = resault.toString();


        if (res.length()>2&&(res.substring(res.length()-2)).equals(".0")){
            res = res.substring(0,(res.length()-2));
        }

        return res;

    }

    private int fillArrays(String unit){

        if (isOperator(unit)) {
            //числовые значения сразу отправляются в rpnArray, а с оператороми все немного сложнее
            //сначала получаем номер операции, она зависит от текущего и предыдущего оператора, получение номера описано в getNumOperation
            NumOperation = getNumOperation(unit);

            //теперь когда номер получен выполняем определенные действия
            if (NumOperation == 1) {
                tempArray.add(unit);// оператов уходит во временную коллекцию
            } else if (NumOperation == 2) {

                //в данной ситуации происходит перенос элемента временной коллекцию в наполняющуюся ОПС
                rpnArray.add(tempArray.get(tempArray.size()-1));
                tempArray.remove(tempArray.size() - 1);

                //в тоже время подаваемый unit пока остается ни куда не определенным
                //поэтому вызываем этот метод снова с тем же unit
                fillArrays(unit);

            } else if (NumOperation == 3) {
                tempArray.remove(tempArray.size() - 1);//удаляем крайний элемент из временного массива
            } else if (NumOperation == 5) {
                //Остановка. Произошла ошибка. Изначальная формула была некорректно сбалансирована
                return 0;//
            }
        } else if(isOperand(unit)){
            rpnArray.add(unit);
        }else{
            //если элемент подаваемой строки не оператор и не может быть конвертирован в тип Double значит выражение не верно
            return 0;
        }
        return 1;
    }

    private int getNumOperation(String currentUnit) {

        //таблица зависимоти номера операции от операторов описана на сайте

        String lastOpertor;

        if (tempArray.size() == 0) {
            lastOpertor = "|";
        } else {
            lastOpertor = tempArray.get(tempArray.size() - 1);
        }

        if (lastOpertor.equals("|")) {
            if (currentUnit.equals("|")) {
                return 4;
            } else if (currentUnit.equals(")")) {
                return 5;
            } else {
                return 1;
            }
        } else if (lastOpertor.equals("+") || lastOpertor.equals("-")) {
            if (currentUnit.equals("*") || lastOpertor.equals("/") || lastOpertor.equals("(")) {
                return 1;
            } else {
                return 2;
            }
        } else if (lastOpertor.equals("*") || lastOpertor.equals("/")) {
            if (currentUnit.equals("(")) {
                return 1;
            } else {
                return 2;
            }
        } else if (lastOpertor.equals("(")) {
            if (currentUnit.equals("|")) {
                return 5;
            } else if (currentUnit.equals(")")) {
                return 3;
            } else {
                return 1;
            }
        };
        return 0;
    }

    private boolean isOperand(String current) {
        try {
            Double.parseDouble(current);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isOperator(String unit) {

        for (String myOperator : myOperators) {

            if (unit.equals(myOperator)) {
                return true;
            }
        }
        return false;
    }

}
