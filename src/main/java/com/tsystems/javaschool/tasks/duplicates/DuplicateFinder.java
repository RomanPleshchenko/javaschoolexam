package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {

        if (sourceFile==null||targetFile==null){
            throw new IllegalArgumentException();
        }

        if (!sourceFile.isFile()) {
            return false;
        }

        //воспользуемся экземпляром класса TreeMap, по двум причинам, 1 - по умолчанию будет сортировка по ключу,
        //2 - в коллекии есть два поля, второе можно будет использовать для хранения колличества повторений.
        TreeMap<String, Integer> map = new TreeMap<String, Integer>();
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(sourceFile));
            String string;
            try {
                while ((string = reader.readLine()) != null) {
                    //если в коллекции не нашли элемента с ключом равным текущей строки добавим новую с значением 1,
                    //если есть, то к полученному значению прибавим 1
                    Integer count = map.get(string);
                    if (count == null) {
                        map.put(string, 1);
                    } else {
                        map.put(string, ++count);
                    }
                }

            } catch (IOException ex) {
                Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Файл не найден");
            return false;
        } finally {
            try {
                reader.close();
            } catch (NullPointerException ex) {
                return false;
            } catch (IOException ex) {
                Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }

        //запишем все строки в файл с помощью,FileWriter второй параметр - true, чтобы строки добавлялись к уже существующим а не перезаписывали весь файл
        try {
            FileWriter writer = new FileWriter(targetFile, true);

            String separator = System.getProperty("line.separator");

            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                String key = entry.getKey();
                Integer value = entry.getValue();

                writer.append(key + "[" + value + "]");
                writer.append(separator);
            }
            writer.flush();
        } catch (IOException ex) {
            Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;//так как все хорошо выполнилось успешно вернем true
    }



}
